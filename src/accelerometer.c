/*
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <stdio.h>
#include <stdlib.h>
#include <zephyr.h>

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(accelerometer);

#include "accelerometer.h"

static const struct device *accelerometer_dev = NULL;

int accelermoter_init()
{
    /* TODO: Update when driver available */
    if ((accelerometer_dev = device_get_binding("SEESAW")) == NULL) {
        LOG_ERR("Accelerometer device not found");
        return -1;
    }

    return 0;
}

int accelerometer_read(struct accelerometer_data *output)
{
    static uint8_t count;

    /* TODO: Update when driver available */
    (*output).x = 30 + count;
    (*output).y = -50 + count * 2;
    (*output).z = 80 - count;

    if (count++ > 100)
        count = 0;

    return 0;
}
