/*
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <zephyr.h>
#ifdef CONFIG_SEESAW_BACKLIGHT
#include "backlight_seesaw.h"
#endif

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(backlight);

#include "backlight.h"

#ifdef CONFIG_SEESAW_BACKLIGHT
static const struct device *seesaw_dev = NULL;
#endif

int backlight_init()
{
#ifdef CONFIG_SEESAW_BACKLIGHT
    if ((seesaw_dev = device_get_binding("SEESAW")) == NULL) {
        LOG_ERR("Backlight device not found");
        return -1;
    }
#endif
    return 0;
}

int backlight_set(uint16_t value)
{
#ifdef CONFIG_SEESAW_BACKLIGHT
    if (seesaw_dev == NULL)
        return -1;
    /* Execute the syscall */
    seesaw_backlight_set_brightness(seesaw_dev, value);
#endif

    return 0;
}
