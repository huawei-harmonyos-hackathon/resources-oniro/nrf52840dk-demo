/*
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _BACKLIGHT_H_
#define _BACKLIGHT_H_

int backlight_init(void);
int backlight_set(uint16_t value);

#endif /* _BACKLIGHT_H_ */
