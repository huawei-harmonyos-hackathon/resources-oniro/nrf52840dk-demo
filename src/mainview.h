/*
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _MAINVIEW_H_
#define _MAINVIEW_H_

int mainview_init(void);
void mainview_update(void);

#endif /* _MAINVIEW_H_ */
