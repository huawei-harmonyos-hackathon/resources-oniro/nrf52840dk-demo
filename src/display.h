/*
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _DISPLAY_H_
#define _DISPLAY_H_

enum disp_blanking_state { DISP_BLANKING_ON = 0, DISP_BLANKING_OFF };

int display_init(void);
void display_blanking_set(enum disp_blanking_state state);

#endif /* _DISPLAY_H_ */
