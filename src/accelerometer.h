/*
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _ACCELEROMETER_H_
#define _ACCELEROMETER_H_

struct accelerometer_data {
    int32_t x;
    int32_t y;
    int32_t z;
};

int accelerometer_init(void);
int accelerometer_read(struct accelerometer_data *output);

#endif /* _ACCELEROMETER_H_ */
