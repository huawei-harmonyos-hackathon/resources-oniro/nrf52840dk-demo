/*
 * SPDX-License-Identifier: Apache-2.0
 */

#include <lv_conf.h>
#include <lvgl.h>
#include <stdio.h>
#include <string.h>

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(mainview);

#include "accelerometer.h"
#include "mainview.h"

#define BAR_HEIGHT 80
#define BAR_WIDTH 30

static lv_obj_t *bar_x;
static lv_obj_t *bar_y;
static lv_obj_t *bar_z;

static lv_obj_t *bar_x_value;
static lv_obj_t *bar_y_value;
static lv_obj_t *bar_z_value;

static void create_view()
{
    lv_obj_t *label_title = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_title, "All Scenarios OS");
    lv_obj_align(label_title, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);

    lv_obj_t *label_x = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_x, "X");
    lv_obj_align(label_x, NULL, LV_ALIGN_IN_LEFT_MID, 19, -35);

    bar_x = lv_bar_create(lv_scr_act(), NULL);
    lv_obj_set_size(bar_x, BAR_WIDTH, BAR_HEIGHT);
    lv_obj_align(bar_x, NULL, LV_ALIGN_IN_LEFT_MID, 9, 20);

    lv_obj_t *label_y = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_y, "Y");
    lv_obj_align(label_y, NULL, LV_ALIGN_IN_LEFT_MID, 59, -35);

    bar_y = lv_bar_create(lv_scr_act(), NULL);
    lv_obj_set_size(bar_y, BAR_WIDTH, BAR_HEIGHT);
    lv_obj_align(bar_y, NULL, LV_ALIGN_IN_LEFT_MID, 49, 20);

    lv_obj_t *label_z = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_z, "Z");
    lv_obj_align(label_z, NULL, LV_ALIGN_IN_LEFT_MID, 99, -35);

    bar_z = lv_bar_create(lv_scr_act(), NULL);
    lv_obj_set_size(bar_z, BAR_WIDTH, BAR_HEIGHT);
    lv_obj_align(bar_z, NULL, LV_ALIGN_IN_LEFT_MID, 89, 20);
    lv_bar_set_anim_time(bar_z, 2000);

    /* Create styles to put colors to XYZ bars */
    static lv_style_t style_x;
    lv_style_init(&style_x);
    lv_style_set_bg_color(&style_x, LV_STATE_DEFAULT, LV_COLOR_OLIVE);
    lv_obj_add_style(bar_x, LV_BAR_PART_INDIC, &style_x);

    static lv_style_t style_y;
    lv_style_init(&style_y);
    lv_style_set_bg_color(&style_y, LV_STATE_DEFAULT, LV_COLOR_TEAL);
    lv_obj_add_style(bar_y, LV_BAR_PART_INDIC, &style_y);

    static lv_style_t style_z;
    lv_style_init(&style_z);
    lv_style_set_bg_color(&style_z, LV_STATE_DEFAULT, LV_COLOR_MAROON);
    lv_obj_add_style(bar_z, LV_BAR_PART_INDIC, &style_z);

    bar_x_value = lv_label_create(lv_scr_act(), NULL);
    static lv_style_t bar_x_value_style;
    lv_style_init(&bar_x_value_style);
    lv_obj_add_style(bar_x_value, LV_STATE_DEFAULT, &bar_x_value_style);
    lv_obj_align(bar_x_value, NULL, LV_ALIGN_IN_LEFT_MID, 12, 70);

    bar_y_value = lv_label_create(lv_scr_act(), NULL);
    static lv_style_t bar_y_value_style;
    lv_style_init(&bar_y_value_style);
    lv_obj_add_style(bar_y_value, LV_STATE_DEFAULT, &bar_y_value_style);
    lv_obj_align(bar_y_value, NULL, LV_ALIGN_IN_LEFT_MID, 52, 70);

    bar_z_value = lv_label_create(lv_scr_act(), NULL);
    static lv_style_t bar_z_value_style;
    lv_style_init(&bar_z_value_style);
    lv_obj_add_style(bar_z_value, LV_STATE_DEFAULT, &bar_z_value_style);
    lv_obj_align(bar_z_value, NULL, LV_ALIGN_IN_LEFT_MID, 92, 70);
}

static void update_view()
{
    struct accelerometer_data data = {0};
    int32_t value[3] = {0};
    lv_obj_t *bar_value[3];
    lv_obj_t *bar[3];

    if (accelerometer_read(&data) != 0)
        return;

    value[0] = data.x;
    value[1] = data.y;
    value[2] = data.z;

    bar[0] = (lv_obj_t *) bar_x;
    bar[1] = (lv_obj_t *) bar_y;
    bar[2] = (lv_obj_t *) bar_z;

    bar_value[0] = (lv_obj_t *) bar_x_value;
    bar_value[1] = (lv_obj_t *) bar_y_value;
    bar_value[2] = (lv_obj_t *) bar_z_value;

    for (int i = 0; i < 3; i++) {
        int32_t cur_val = value[i];
        char cur_str[10] = {0};

        snprintf(cur_str, sizeof(cur_str), "%d", cur_val);
        lv_label_set_text(bar_value[i], cur_str);
        lv_bar_set_value(bar[i], cur_val, LV_ANIM_OFF);

        if (cur_val < 0) {
            cur_val = cur_val * (-1);
            cur_val = MAX(cur_val, 0);
            cur_val = MIN(cur_val, 100);
            lv_bar_set_value(bar[i], cur_val, LV_ANIM_OFF);
        } else {
            cur_val = MIN(cur_val, 100);
            lv_bar_set_value(bar[i], cur_val, LV_ANIM_OFF);
        }
    }
}

int mainview_init()
{
    create_view();
    lv_task_handler();

    return 0;
}

void mainview_update()
{
    update_view();
    lv_task_handler();
}
