/*
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>

#include "backlight.h"
#include "display.h"
#include "mainview.h"

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(app);

static int hardware_init()
{
    if (display_init() < 0)
        return -1;

    if (backlight_init() < 0)
        return -1;

    if (backlight_set(0xA000) < 0)
        return -1;

    return 0;
}

void main(void)
{
    LOG_INF("Main application started");

    if (hardware_init() != 0) {
        LOG_ERR("Hardware init failed");
        return;
    }

    if (mainview_init() != 0) {
        LOG_ERR("Mainview init failed");
        return;
    }

    /* turn on display */
    display_blanking_set(DISP_BLANKING_OFF);

    while (1) {
        mainview_update();
        k_sleep(K_MSEC(10));
    }
}
