/*
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <drivers/display.h>
#include <zephyr.h>

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(display);

#include "display.h"

static const struct device *display_dev = NULL;

int display_init()
{
    display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);
    if (display_dev == NULL) {
        LOG_ERR("Display device not found");
        return -1;
    }

    return 0;
}

void display_blanking_set(enum disp_blanking_state state)
{
    if (state == DISP_BLANKING_ON)
        display_blanking_on(display_dev);
    else
        display_blanking_off(display_dev);
}
