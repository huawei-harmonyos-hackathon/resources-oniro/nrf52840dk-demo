# NRF52840dk Demo
Small Zephyr flavor for NRF52840-DK Hardware

## Description
The projects provides an example of Zephyr flavor project integration in Oniro.
It add new device driver for NRF52840-DK hardware and Arduino display shield.
To demonstrate the usage of Zephyr modules the LVGL is used to animate x,y,z axis of an accelerometer.
