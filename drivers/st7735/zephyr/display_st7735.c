/*
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT sitronix_st7735

#include "display_st7735.h"

#include <device.h>
#include <drivers/display.h>
#include <drivers/gpio.h>
#include <drivers/spi.h>
#include <sys/byteorder.h>

#include <logging/log.h>
LOG_MODULE_REGISTER(display_st7735);

#define ST7735_DISPLAY_CS_PIN DT_INST_SPI_DEV_CS_GPIOS_PIN(0)
#define ST7735_DISPLAY_CMD_DATA_PIN DT_INST_GPIO_PIN(0, cmd_data_gpios)
#define ST7735_DISPLAY_CMD_DATA_FLAGS DT_INST_GPIO_FLAGS(0, cmd_data_gpios)
#define ST7735_DISPLAY_RESET_PIN DT_INST_GPIO_PIN(0, reset_gpios)
#define ST7735_DISPLAY_RESET_FLAGS DT_INST_GPIO_FLAGS(0, reset_gpios)

static uint8_t st7735_display_frmctr1_param[] = DT_INST_PROP(0, frmctr1_param);
static uint8_t st7735_display_frmctr2_param[] = DT_INST_PROP(0, frmctr2_param);
static uint8_t st7735_display_frmctr3_param[] = DT_INST_PROP(0, frmctr3_param);
static uint8_t st7735_display_pwctr1_param[] = DT_INST_PROP(0, pwctr1_param);
static uint8_t st7735_display_pwctr2_param[] = DT_INST_PROP(0, pwctr2_param);
static uint8_t st7735_display_pwctr3_param[] = DT_INST_PROP(0, pwctr3_param);
static uint8_t st7735_display_pwctr4_param[] = DT_INST_PROP(0, pwctr4_param);
static uint8_t st7735_display_pwctr5_param[] = DT_INST_PROP(0, pwctr5_param);
static uint8_t st7735_display_caset_param[] = DT_INST_PROP(0, caset_param);
static uint8_t st7735_display_raset_param[] = DT_INST_PROP(0, raset_param);
static uint8_t st7735_display_gmctrp1_param[] = DT_INST_PROP(0, gmctrp1_param);
static uint8_t st7735_display_gmctrn1_param[] = DT_INST_PROP(0, gmctrn1_param);

struct st7735_display_data {
    const struct device *spi_dev;
    struct spi_config spi_config;
#if DT_INST_SPI_DEV_HAS_CS_GPIOS(0)
    struct spi_cs_control cs_ctrl;
#endif
#if DT_INST_NODE_HAS_PROP(0, reset_gpios)
    const struct device *reset_gpio;
#endif
    const struct device *cmd_data_gpio;
    uint16_t height;
    uint16_t width;
    uint16_t x_offset;
    uint16_t y_offset;
#ifdef CONFIG_PM_DEVICE
    uint32_t pm_state;
#endif
};

static struct st7735_display_data st7735_display_data = {
    .width = DT_INST_PROP(0, width),
    .height = DT_INST_PROP(0, height),
    .x_offset = DT_INST_PROP(0, x_offset),
    .y_offset = DT_INST_PROP(0, y_offset),
};

static void st7735_display_set_cmd(struct st7735_display_data *data, int is_cmd)
{
    gpio_pin_set(data->cmd_data_gpio, ST7735_DISPLAY_CMD_DATA_PIN, is_cmd);
}

static void st7735_display_transmit(struct st7735_display_data *data,
                                    uint8_t cmd,
                                    uint8_t *tx_data,
                                    size_t tx_count)
{
    struct spi_buf tx_buf = {.buf = &cmd, .len = 1};
    struct spi_buf_set tx_bufs = {.buffers = &tx_buf, .count = 1};

    st7735_display_set_cmd(data, 1);
    spi_write(data->spi_dev, &data->spi_config, &tx_bufs);
    st7735_display_set_cmd(data, 0);

    LOG_DBG("Transmit cmd=0x%x data=%p len=%d", cmd, tx_data, tx_count);
    if (tx_data != NULL) {
        tx_buf.buf = tx_data;
        tx_buf.len = tx_count;
        spi_write(data->spi_dev, &data->spi_config, &tx_bufs);
    }
}

static void st7735_display_exit_sleep(struct st7735_display_data *data)
{
    st7735_display_transmit(data, ST7735_DISPLAY_SLPOUT, NULL, 0);
    k_sleep(K_MSEC(120));
}

static void st7735_display_reset_display(struct st7735_display_data *data)
{
    LOG_INF("Resetting display");
#if DT_INST_NODE_HAS_PROP(0, reset_gpios)
    k_sleep(K_MSEC(1));
    gpio_pin_set(data->reset_gpio, ST7735_DISPLAY_RESET_PIN, 1);
    k_sleep(K_MSEC(6));
    gpio_pin_set(data->reset_gpio, ST7735_DISPLAY_RESET_PIN, 0);
    k_sleep(K_MSEC(20));
#else
    st7735_display_transmit(data, ST7735_DISPLAY_SWRESET, NULL, 0);
    k_sleep(K_MSEC(5));
#endif
}

static void st7735_display_lcd_init(struct st7735_display_data *data)
{
    uint8_t buf = 0;

    /* SWRESET */
    st7735_display_transmit(data, ST7735_DISPLAY_SWRESET, NULL, 0);
    k_sleep(K_MSEC(100));

    /* SLPOUT */
    st7735_display_transmit(data, ST7735_DISPLAY_SLPOUT, NULL, 0);
    k_sleep(K_MSEC(200));

    /* FRMCTR */
    st7735_display_transmit(data,
                            ST7735_DISPLAY_FRMCTR1,
                            st7735_display_frmctr1_param,
                            sizeof(st7735_display_frmctr1_param));
    st7735_display_transmit(data,
                            ST7735_DISPLAY_FRMCTR2,
                            st7735_display_frmctr2_param,
                            sizeof(st7735_display_frmctr2_param));
    st7735_display_transmit(data,
                            ST7735_DISPLAY_FRMCTR3,
                            st7735_display_frmctr3_param,
                            sizeof(st7735_display_frmctr3_param));

    /* INVCTR */
    buf = 0x07;
    st7735_display_transmit(data, ST7735_DISPLAY_INVCTR, &buf, 1);

    /* PWCTR */
    st7735_display_transmit(data,
                            ST7735_DISPLAY_PWCTR1,
                            st7735_display_pwctr1_param,
                            sizeof(st7735_display_pwctr1_param));
    st7735_display_transmit(data,
                            ST7735_DISPLAY_PWCTR2,
                            st7735_display_pwctr2_param,
                            sizeof(st7735_display_pwctr2_param));
    st7735_display_transmit(data,
                            ST7735_DISPLAY_PWCTR3,
                            st7735_display_pwctr3_param,
                            sizeof(st7735_display_pwctr3_param));
    st7735_display_transmit(data,
                            ST7735_DISPLAY_PWCTR4,
                            st7735_display_pwctr4_param,
                            sizeof(st7735_display_pwctr4_param));
    st7735_display_transmit(data,
                            ST7735_DISPLAY_PWCTR5,
                            st7735_display_pwctr5_param,
                            sizeof(st7735_display_pwctr5_param));

    /* VMCTR1 */
    buf = 0x0E;
    st7735_display_transmit(data, ST7735_DISPLAY_VMCTR1, &buf, 1);

    /* INVOFF */
    st7735_display_transmit(data, ST7735_DISPLAY_INVOFF, NULL, 0);

    /* MADCTL */
    buf = 0xC8;
    st7735_display_transmit(data, ST7735_DISPLAY_MADCTL, &buf, 1);

    /* COLMOD */
    buf = 0x05;
    st7735_display_transmit(data, ST7735_DISPLAY_COLMOD, &buf, 1);

    /* CASET */
    st7735_display_transmit(
        data, ST7735_DISPLAY_CASET, st7735_display_caset_param, sizeof(st7735_display_caset_param));

    /* RASET */
    st7735_display_transmit(
        data, ST7735_DISPLAY_RASET, st7735_display_raset_param, sizeof(st7735_display_raset_param));

    /* GMCTRP1 */
    st7735_display_transmit(data,
                            ST7735_DISPLAY_GMCTRP1,
                            st7735_display_gmctrp1_param,
                            sizeof(st7735_display_gmctrp1_param));

    /* GMCTRN1 */
    st7735_display_transmit(data,
                            ST7735_DISPLAY_GMCTRP1,
                            st7735_display_gmctrn1_param,
                            sizeof(st7735_display_gmctrn1_param));

    /* NORON */
    st7735_display_transmit(data, ST7735_DISPLAY_NORON, NULL, 0);
    k_sleep(K_MSEC(10));

    /* DISPON */
    st7735_display_transmit(data, ST7735_DISPLAY_DISPON, NULL, 0);
    k_sleep(K_MSEC(100));
}

static void st7735_display_set_mem_area(struct st7735_display_data *data,
                                        const uint16_t x,
                                        const uint16_t y,
                                        const uint16_t w,
                                        const uint16_t h)
{
    uint16_t spi_data[2];
    uint16_t ram_x = x + data->x_offset;
    uint16_t ram_y = y + data->y_offset;

    LOG_DBG("Set area %dx%d (x,y) @ %dx%d (w,h)", x, y, w, h);

    spi_data[0] = sys_cpu_to_be16(ram_x);
    spi_data[1] = sys_cpu_to_be16(ram_x + w - 1);
    st7735_display_transmit(data, ST7735_DISPLAY_CASET, (uint8_t *) &spi_data[0], 4);

    spi_data[0] = sys_cpu_to_be16(ram_y);
    spi_data[1] = sys_cpu_to_be16(ram_y + h - 1);
    st7735_display_transmit(data, ST7735_DISPLAY_RASET, (uint8_t *) &spi_data[0], 4);
}

static int st7735_display_write(const struct device *dev,
                                const uint16_t x,
                                const uint16_t y,
                                const struct display_buffer_descriptor *desc,
                                const void *buf)
{
    struct st7735_display_data *data = (struct st7735_display_data *) dev->data;
    const uint8_t *write_data_start = (uint8_t *) buf;
    struct spi_buf tx_buf;
    struct spi_buf_set tx_bufs;
    uint16_t write_cnt;
    uint16_t nbr_of_writes;
    uint16_t write_h;

    __ASSERT(desc->width <= desc->pitch, "Pitch is smaller then width");
    __ASSERT((desc->pitch * ST7735_DISPLAY_PIXEL_SIZE * desc->height) <= desc->buf_size,
             "Input buffer to small");

    st7735_display_set_mem_area(data, x, y, desc->width, desc->height);

    LOG_DBG("Writing %dx%d (w,h) @ %dx%d (x,y)", desc->width, desc->height, x, y);

    if (desc->pitch > desc->width) {
        write_h = 1U;
        nbr_of_writes = desc->height;
    } else {
        write_h = desc->height;
        nbr_of_writes = 1U;
    }

    st7735_display_transmit(data,
                            ST7735_DISPLAY_RAMWR,
                            (void *) write_data_start,
                            desc->width * ST7735_DISPLAY_PIXEL_SIZE * write_h);

    tx_bufs.buffers = &tx_buf;
    tx_bufs.count = 1;

    write_data_start += (desc->pitch * ST7735_DISPLAY_PIXEL_SIZE);
    for (write_cnt = 1U; write_cnt < nbr_of_writes; ++write_cnt) {
        tx_buf.buf = (void *) write_data_start;
        tx_buf.len = desc->width * ST7735_DISPLAY_PIXEL_SIZE * write_h;
        spi_write(data->spi_dev, &data->spi_config, &tx_bufs);
        write_data_start += (desc->pitch * ST7735_DISPLAY_PIXEL_SIZE);
    }

    return 0;
}

static int st7735_display_read(const struct device *dev,
                               const uint16_t x,
                               const uint16_t y,
                               const struct display_buffer_descriptor *desc,
                               void *buf)
{
    return -ENOTSUP;
}

static void *st7735_display_get_framebuffer(const struct device *dev)
{
    return NULL;
}

static int st7735_display_blanking_off(const struct device *dev)
{
    struct st7735_display_data *data = (struct st7735_display_data *) dev->data;

    st7735_display_transmit(data, ST7735_DISPLAY_DISPON, NULL, 0);
    return 0;
}

static int st7735_display_blanking_on(const struct device *dev)
{
    struct st7735_display_data *data = (struct st7735_display_data *) dev->data;

    st7735_display_transmit(data, ST7735_DISPLAY_DISPOFF, NULL, 0);
    return 0;
}

static int st7735_display_set_brightness(const struct device *dev, const uint8_t brightness)
{
    return -ENOTSUP;
}

static int st7735_display_set_contrast(const struct device *dev, const uint8_t contrast)
{
    return -ENOTSUP;
}

static void st7735_display_get_capabilities(const struct device *dev,
                                            struct display_capabilities *capabilities)
{
    struct st7735_display_data *data = (struct st7735_display_data *) dev->data;

    memset(capabilities, 0, sizeof(struct display_capabilities));
    capabilities->x_resolution = data->width;
    capabilities->y_resolution = data->height;
    capabilities->supported_pixel_formats = PIXEL_FORMAT_RGB_565;
    capabilities->current_pixel_format = PIXEL_FORMAT_RGB_565;
    capabilities->current_orientation = DISPLAY_ORIENTATION_NORMAL;
}

static int st7735_display_set_pixel_format(const struct device *dev,
                                           const enum display_pixel_format pixel_format)
{
    return 0; /* statically set to PIXEL_FORMAT_RGB_565 */
}

#ifdef CONFIG_PM_DEVICE
static void st7735_display_enter_sleep(struct st7735_display_data *data)
{
    st7735_display_transmit(data, ST7735_DISPLAY_SLPIN, NULL, 0);
}

static int st7735_display_pm_control(const struct device *dev,
                                     uint32_t ctrl_command,
                                     void *context,
                                     device_pm_cb cb,
                                     void *arg)
{
    int ret = 0;
    struct st7735_display_data *data = (struct st7735_display_data *) dev->data;

    switch (ctrl_command) {
    case DEVICE_PM_SET_POWER_STATE:
        if (*((uint32_t *) context) == DEVICE_PM_ACTIVE_STATE) {
            st7735_display_exit_sleep(data);
            data->pm_state = DEVICE_PM_ACTIVE_STATE;
            ret = 0;
        } else {
            st7735_display_enter_sleep(data);
            data->pm_state = DEVICE_PM_LOW_POWER_STATE;
            ret = 0;
        }
        break;
    case DEVICE_PM_GET_POWER_STATE:
        *((uint32_t *) context) = data->pm_state;
        break;
    default:
        ret = -EINVAL;
    }

    if (cb != NULL) {
        cb(dev, ret, context, arg);
    }

    return ret;
}
#endif /* CONFIG_PM_DEVICE */

static int st7735_display_init(const struct device *dev)
{
    struct st7735_display_data *data = (struct st7735_display_data *) dev->data;

    data->spi_dev = device_get_binding(DT_INST_BUS_LABEL(0));
    if (data->spi_dev == NULL) {
        LOG_ERR("Could not get SPI device for LCD");
        return -EPERM;
    }

    data->spi_config.frequency = DT_INST_PROP(0, spi_max_frequency);
    data->spi_config.operation = SPI_OP_MODE_MASTER | SPI_WORD_SET(8);
    data->spi_config.slave = DT_INST_REG_ADDR(0);

#if DT_INST_SPI_DEV_HAS_CS_GPIOS(0)
    data->cs_ctrl.gpio_dev = device_get_binding(DT_INST_SPI_DEV_CS_GPIOS_LABEL(0));
    data->cs_ctrl.gpio_pin = DT_INST_SPI_DEV_CS_GPIOS_PIN(0);
    data->cs_ctrl.gpio_dt_flags = DT_INST_SPI_DEV_CS_GPIOS_FLAGS(0);
    data->cs_ctrl.delay = 10U;
    data->spi_config.cs = &(data->cs_ctrl);
#else
    data->spi_config.cs = NULL;
#endif

#ifdef CONFIG_PM_DEVICE
    data->pm_state = DEVICE_PM_ACTIVE_STATE;
#endif

#if DT_INST_NODE_HAS_PROP(0, reset_gpios)
    data->reset_gpio = device_get_binding(DT_INST_GPIO_LABEL(0, reset_gpios));
    if (data->reset_gpio == NULL) {
        LOG_ERR("Could not get GPIO port for display reset");
        return -EPERM;
    }

    if (gpio_pin_configure(data->reset_gpio,
                           ST7735_DISPLAY_RESET_PIN,
                           GPIO_OUTPUT_INACTIVE | ST7735_DISPLAY_RESET_FLAGS)) {
        LOG_ERR("Couldn't configure reset pin");
        return -EIO;
    }
#endif

    /* Configure cmd/data port */
    data->cmd_data_gpio = device_get_binding(DT_INST_GPIO_LABEL(0, cmd_data_gpios));
    if (data->cmd_data_gpio == NULL) {
        LOG_ERR("Could not get GPIO port for cmd/DATA port");
        return -EPERM;
    }
    if (gpio_pin_configure(data->cmd_data_gpio,
                           ST7735_DISPLAY_CMD_DATA_PIN,
                           GPIO_OUTPUT | ST7735_DISPLAY_CMD_DATA_FLAGS)) {
        LOG_ERR("Couldn't configure cmd/DATA pin");
        return -EIO;
    }

    /* Init display */
    LOG_INF("ST7735 display init");
    st7735_display_reset_display(data);
    st7735_display_blanking_on(dev);
    st7735_display_lcd_init(data);
    st7735_display_exit_sleep(data);

    return 0;
}

static const struct display_driver_api st7735_display_display_api = {
    .blanking_on = st7735_display_blanking_on,
    .blanking_off = st7735_display_blanking_off,
    .write = st7735_display_write,
    .read = st7735_display_read,
    .get_framebuffer = st7735_display_get_framebuffer,
    .set_brightness = st7735_display_set_brightness,
    .set_contrast = st7735_display_set_contrast,
    .get_capabilities = st7735_display_get_capabilities,
    .set_pixel_format = st7735_display_set_pixel_format,
};

DEVICE_DT_INST_DEFINE(0,
                      &st7735_display_init,
                      NULL,
                      &st7735_display_data,
                      NULL,
                      POST_KERNEL,
                      CONFIG_APPLICATION_INIT_PRIORITY,
                      &st7735_display_display_api);
