/*
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _SEESAW_BACKLIGHT_DRIVER_H_
#define _SEESAW_BACKLIGHT_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <device.h>
#include <drivers/i2c.h>

#define SEESAW_I2C_ADDR 0x2E

/* status module function address registers */
enum {
    SEESAW_STATUS_HW_ID = 0x01,
    SEESAW_STATUS_VERSION = 0x02,
    SEESAW_STATUS_OPTIONS = 0x03,
    SEESAW_STATUS_TEMP = 0x04,
    SEESAW_STATUS_SWRST = 0x7F,
};

/* The module base addresses for different seesaw modules */
enum {
    SEESAW_STATUS_BASE = 0x00,
    SEESAW_GPIO_BASE = 0x01,
    SEESAW_SERCOM0_BASE = 0x02,
    SEESAW_TIMER_BASE = 0x08,
    SEESAW_ADC_BASE = 0x09,
    SEESAW_DAC_BASE = 0x0A,
    SEESAW_INTERRUPT_BASE = 0x0B,
    SEESAW_DAP_BASE = 0x0C,
    SEESAW_EEPROM_BASE = 0x0D,
    SEESAW_NEOPIXEL_BASE = 0x0E,
    SEESAW_TOUCH_BASE = 0x0F,
    SEESAW_KEYPAD_BASE = 0x10,
    SEESAW_ENCODER_BASE = 0x11,
};

/* GPIO module function addres registers */
enum {
    SEESAW_GPIO_DIRSET_BULK = 0x02,
    SEESAW_GPIO_DIRCLR_BULK = 0x03,
    SEESAW_GPIO_BULK = 0x04,
    SEESAW_GPIO_BULK_SET = 0x05,
    SEESAW_GPIO_BULK_CLR = 0x06,
    SEESAW_GPIO_BULK_TOGGLE = 0x07,
    SEESAW_GPIO_INTENSET = 0x08,
    SEESAW_GPIO_INTENCLR = 0x09,
    SEESAW_GPIO_INTFLAG = 0x0A,
    SEESAW_GPIO_PULLENSET = 0x0B,
    SEESAW_GPIO_PULLENCLR = 0x0C,
};

/* Timer module function address registers */
enum {
    SEESAW_TIMER_STATUS = 0x00,
    SEESAW_TIMER_PWM = 0x01,
    SEESAW_TIMER_FREQ = 0x02,
};

__subsystem struct seesaw_backlight_driver_api {
    void (*set_brightness)(const struct device *dev, uint16_t state);
};

__syscall void seesaw_backlight_set_brightness(const struct device *dev, uint16_t state);

static inline void z_impl_seesaw_backlight_set_brightness(const struct device *dev, uint16_t state)
{
    const struct seesaw_backlight_driver_api *api = dev->api;

    __ASSERT(api->print, "Callback pointer should not be NULL");
    api->set_brightness(dev, state);
}

#ifdef __cplusplus
}
#endif

#include <syscalls/backlight_seesaw.h>

#endif /* _SEESAW_BACKLIGHT_DRIVER_H_ */
