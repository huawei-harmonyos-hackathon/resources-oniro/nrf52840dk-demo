/*
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT arduino_seesaw

#include "backlight_seesaw.h"

#include <device.h>
#include <drivers/i2c.h>
#include <syscall_handler.h>
#include <zephyr/types.h>

#define LOG_LEVEL CONFIG_SEESAW_BACKLIGHT_LOG_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(backlight_seesaw);

struct seesaw_backlight_data {
    const struct device *i2c_dev;
    uint16_t i2c_slave_addr;
    uint8_t hardware_id;
};

static struct seesaw_backlight_data seesaw_backlight_data = {
    .i2c_dev = NULL,
    .i2c_slave_addr = SEESAW_I2C_ADDR,
    .hardware_id = DT_INST_PROP(0, hardware_id),
};

static int i2c_write_bytes(const struct device *dev, uint16_t addr, uint8_t *buf, uint32_t len)
{
    struct seesaw_backlight_data *data = (struct seesaw_backlight_data *) dev->data;
    struct i2c_msg msgs[2] = {0};
    uint8_t wd_addr[2] = {0};

    wd_addr[0] = (addr >> 8) & 0xFF;
    wd_addr[1] = addr & 0xFF;

    msgs[0].buf = wd_addr;
    msgs[0].len = 2U;
    msgs[0].flags = I2C_MSG_WRITE;

    msgs[1].buf = buf;
    msgs[1].len = len;
    msgs[1].flags = I2C_MSG_WRITE | I2C_MSG_STOP;

    return i2c_transfer(data->i2c_dev, &msgs[0], 2, data->i2c_slave_addr);
}

static int i2c_read_bytes(const struct device *dev, uint16_t addr, uint8_t *buf, uint32_t len)
{
    struct seesaw_backlight_data *data = (struct seesaw_backlight_data *) dev->data;
    struct i2c_msg msgs[2] = {0};
    uint8_t wd_addr[2] = {0};

    wd_addr[0] = (addr >> 8) & 0xFF;
    wd_addr[1] = addr & 0xFF;

    msgs[0].buf = wd_addr;
    msgs[0].len = 2U;
    msgs[0].flags = I2C_MSG_WRITE;

    msgs[1].buf = buf;
    msgs[1].len = len;
    msgs[1].flags = I2C_MSG_RESTART | I2C_MSG_READ | I2C_MSG_STOP;

    return i2c_transfer(data->i2c_dev, &msgs[0], 2, data->i2c_slave_addr);
}

static int init_backlight_seesaw(const struct device *dev)
{
    uint16_t reg = ((SEESAW_STATUS_BASE << 8) & 0xFF00) | (SEESAW_STATUS_SWRST & 0xFF);
    uint8_t buf[] = {0xFF};

    return i2c_write_bytes(dev, reg, &buf[0], 1);
}

static uint8_t get_hardware_id(const struct device *dev, uint8_t *hw_id)
{
    uint16_t reg = ((SEESAW_STATUS_BASE << 8) & 0xFF00) | (SEESAW_STATUS_HW_ID & 0xFF);

    return i2c_read_bytes(dev, reg, hw_id, 1);
}

static int set_brightness(const struct device *dev, uint16_t value)
{
    uint16_t reg = (uint16_t)((SEESAW_TIMER_BASE << 8) & 0xFF00) | (SEESAW_TIMER_PWM & 0xFF);
    uint8_t buf[] = {0x00, (uint8_t)(value >> 8), (uint8_t) value};

    LOG_DBG("Backlight set value=0x%x", value);
    return i2c_write_bytes(dev, reg, &buf[0], 3);
}

static int seesaw_backlight_init(const struct device *dev)
{
    struct seesaw_backlight_data *data = (struct seesaw_backlight_data *) dev->data;
    int hw_id_retry = 0;
    uint8_t hw_id = 0;
    int status = 0;

    data->i2c_dev = device_get_binding(DT_INST_BUS_LABEL(0));
    if (data->i2c_dev == NULL) {
        LOG_ERR("Device not found");
        return -EPERM;
    }

    data->i2c_slave_addr = DT_INST_REG_ADDR(0);

    if ((status = init_backlight_seesaw(dev)) != 0) {
        LOG_ERR("Failed to init backlight status=%d", status);
        return status;
    }

    do {
        if ((status = get_hardware_id(dev, &hw_id)) != 0) {
            LOG_ERR("Failed read hardware id, status=%d", status);
            return status;
        }
        k_sleep(K_MSEC(10));
    } while ((hw_id_retry++ < 10) && (hw_id != data->hardware_id));

    if (hw_id != data->hardware_id) {
        LOG_ERR("Hardware ID not match. Read ID=%d", hw_id);
        return -ENODEV;
    }

    LOG_INF("Backlight hw=0x%x slave=0x%x", hw_id, data->i2c_slave_addr);

    return status;
}

static void seesaw_backlight_set_brightness_impl(const struct device *dev, uint16_t state)
{
    if (set_brightness(dev, state) != 0)
        LOG_ERR("Failed to set backlight");
}

#ifdef CONFIG_USERSPACE
static inline void z_vrfy_seesaw_backlight_set_brightness(const struct device *dev, uint16_t state)
{
    Z_OOPS(Z_SYSCALL_SEESAW_BACKLIGHT(dev, seesaw_backlight_set_brightness));
    z_impl_seesaw_backlight_set_brightness(dev, state);
}
#include <syscalls/seesaw_backlight_set_mrsh.c>
#endif /* CONFIG_USERSPACE */

static const struct seesaw_backlight_driver_api seesaw_backlight_driver_api
    = {.set_brightness = seesaw_backlight_set_brightness_impl};

DEVICE_DT_INST_DEFINE(0,
                      &seesaw_backlight_init,
                      NULL,
                      &seesaw_backlight_data,
                      NULL,
                      POST_KERNEL,
                      CONFIG_APPLICATION_INIT_PRIORITY,
                      &seesaw_backlight_driver_api);
